<?php

namespace Netrunnerdb\CardsBundle\Entity;

use Gedmo\Translatable\Translatable;

/**
 * Cycle
 */
class Cycle implements Translatable
{
    public function toString() {
		return $this->name;
	}
	
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $name;

    /**
     * @var integer
     */
    private $position;

    /**
     * @var integer
     */
    private $size;
    
    /**
     * @var string
     */
    private $locale = 'en';
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Cycle
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Cycle
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
    	return $this->name;
    }

    /**
     * Set position
     *
     * @param integer $position
     * @return Cycle
     */
    public function setPosition($position)
    {
        $this->position = $position;
    
        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }
    
    /**
     * Set size
     *
     * @param integer $size
     * @return Cycle
     */
    public function setSize($size)
    {
    	$this->size = $size;
    
    	return $this;
    }
    
    /**
     * Get size
     *
     * @return integer
     */
    public function getSize()
    {
    	return $this->size;
    }
    
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $packs;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->packs = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add packs
     *
     * @param \Netrunnerdb\CardsBundle\Entity\Pack $packs
     * @return Cycle
     */
    public function addPack(\Netrunnerdb\CardsBundle\Entity\Pack $packs)
    {
        $this->packs[] = $packs;
    
        return $this;
    }

    /**
     * Remove packs
     *
     * @param \Netrunnerdb\CardsBundle\Entity\Pack $packs
     */
    public function removePack(\Netrunnerdb\CardsBundle\Entity\Pack $packs)
    {
        $this->packs->removeElement($packs);
    }

    /**
     * Get packs
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPacks()
    {
        return $this->packs;
    }
    /**
     * @var string
     */
    private $nameIt;


    /**
     * Set nameIt
     *
     * @param string $nameIt
     * @return Cycle
     */
    public function setNameIt($nameIt)
    {
        $this->nameIt = $nameIt;

        return $this;
    }

    /**
     * Get nameIt
     *
     * @return string 
     */
    public function getNameIt()
    {
        return $this->nameIt;
    }

    public function setTranslatableLocale($locale)
    {
    	$this->locale = $locale;
    }
}
